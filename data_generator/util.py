import json
import num2words
import string
import re
import pickle
import datetime
from gensim.models import KeyedVectors
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize




def clean_text(context):
    stopWords = set(stopwords.words('english'))
    text = re.sub(r"(\d+)", lambda x: num2words.num2words(int(x.group(0))) + ' ', context)
    text = text.translate(str.maketrans('', '', string.punctuation))
    text = text.replace('\n','')
    text = text.replace('“','')
    text = text.replace('”','')
    text = ' '.join([word for word in word_tokenize(text) if word not in stopWords])
    return text


def get_stock_info(ticker):
    raw_price_file = './data_generator/data/stockPrices_raw.json'
    price_return_file = './data_generator/data/stockReturns.json'

    with open(raw_price_file) as file:
        print("Loading price info ... ")
        priceDt = json.load(file)


    dateSet = priceDt['^GSPC']['adjClose'].keys()

    stock_info_dict = {}

    for num, ty in enumerate(priceDt[ticker]):
        for date in dateSet:
            convert_date = datetime.datetime.strptime(date, "%Y-%m-%d").strftime("%Y%m%d")

            if convert_date not in stock_info_dict:
                stock_info_dict[convert_date] = []
                stock_info_dict[convert_date].append(priceDt[ticker][ty][date])
            else:
                stock_info_dict[convert_date].append(priceDt[ticker][ty][date])

    with open(price_return_file) as file:
        print("Loading price return info ... ")
        priceDt = json.load(file)

    for num, term in enumerate(priceDt):
        for date in dateSet:
            convert_date = datetime.datetime.strptime(date, "%Y-%m-%d").strftime("%Y%m%d")
            price = priceDt[term][ticker][convert_date]
            stock_info_dict[convert_date].append(price)

    return stock_info_dict


def load_decomposed_vector():

    with open('./data_generator/data/decomposed_vector','rb') as f:
        decomposed_vector = pickle.load(f)

    return decomposed_vector




def get_google_model():
    word_vector = {}
    wv_from_bin = KeyedVectors.load_word2vec_format("../GoogleNews-vectors-negative300.bin", binary=True, limit = 30000)
    for word, _ in wv_from_bin.vocab.items():
        word_vector[word] = wv_from_bin.get_vector(word)
    return word_vector