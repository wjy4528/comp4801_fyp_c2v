from generate_training_matrix import DataGenerator
from os import listdir
from os.path import isfile, join
import argparse


NEWS_TOPIC = ['us-economy','energy','commodities','financials','retail-consumer','currencies']

def main():
    parser = argparse.ArgumentParser(description='Training Data Generator')
    parser.add_argument('-type', type=str, default = 'single', help='Generate Data Type: [single][sentiment][headline][content][highdi]')
    parser.add_argument('-USER_INPUT', type=str, default = './data/user_input.csv', help='User input file name')
    parser.add_argument('-ticker', type=str, default = 'IYE', help='Target Stock Ticker')
    parser.add_argument('-NEWS_DIR', type=str, default = './data/financial_times', help='Data Dir, default : ./data/financial_times')
    
    args = parser.parse_args()

    files = [join(args.NEWS_DIR, f) for f in listdir(args.NEWS_DIR) if isfile(join(args.NEWS_DIR, f))]
    dg = DataGenerator(args.ticker, files)

    if args.type == 'single':

        dg = DataGenerator(args.ticker, [args.USER_INPUT])        
        print(dg.generate_c2v_content_sentiment()) # return np.matrix


    elif args.type == 'sentiment':        
        dg.generate_sentiment_matrix()

    elif args.type == 'headline':        
        dg.generate_c2v_headline_sentiment()

    elif args.type == 'content':       
        dg.generate_c2v_content_sentiment()

    elif args.type == 'highdi':
        dg.generate_high_dimension_w2v()

    else:
        print('Not support type')
        raise SyntaxError
    
if __name__ == '__main__':
    main()