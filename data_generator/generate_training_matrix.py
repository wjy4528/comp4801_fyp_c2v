from nltk.sentiment.vader import SentimentIntensityAnalyzer
import csv
from dateutil import parser
import numpy as np
import collections
from .word_selection import Word_Selector
import datetime
from . import util
from tqdm import tqdm
import random


class DataGenerator():
    def __init__(self, ticker, files):
        self.sia = SentimentIntensityAnalyzer()
        self.ticker = ticker
        self.files = files




    def generate_sentiment_matrix(self):
        date_news = {}
        headlines = []
        features = []

        for file in self.files:
            for row in csv.reader(open(file, 'r', encoding = 'utf-8')):
                dt = parser.parse(row[1])
                date = dt.strftime("%Y%m%d")
                headline = clean_text(row[0])
                #print(headline)
                if date in date_news:
                    date_news[date] += (' ' + headline)
                else:
                    date_news[date] = headline

        ordered_news = collections.OrderedDict(sorted(date_news.items()))
        stock_info = get_stock_info(self.ticker)

        for news_date in ordered_news:
            sentiment_score = self.sia.polarity_scores(ordered_news[news_date])['compound']
            if news_date in stock_info:
                features.append([sentiment_score] + stock_info[news_date])

        features = np.matrix(features)
        print(features.shape)
        with open('./data_generator/output/sentiment_matrix', 'w') as file:

            np.savetxt(file, features, fmt = "%s", delimiter=",", header=\
                "SENTIMENT, OPEN, HIGH, LOW, CLOSE, ADJCLOSE, VOL, S_TERM, M_TERM, L_TERM")


    def generate_c2v_headline_sentiment(self):


        decomposed_vector = util.load_decomposed_vector()

        word_selector = Word_Selector()

        stock_info = util.get_stock_info(self.ticker)

        features = []

        for file in self.files:
            for row in csv.reader(open(file, 'r', encoding = 'utf-8')):

                date = parser.parse(row[1]).strftime("%Y%m%d") #date = 'YYYYMMDD'
                #feature shape ===> (20, 1)
                #feature format {word_vec, ...(20).... , word_vec, sentiment_score, short_term_return, mid_term_return, long_term_return , raw_stock_open_adj_close }
                #print(date)


                if date in stock_info:
                    #print(date)
                    feature = []

                    selected_content   = word_selector.get_top_freq(decomposed_vector, row[2], 10)

                    if selected_content:

                        selected_sentiment = util.clean_text(row[0])  #headline, content

                        for words in selected_content:
                            #print(words)
                            feature.append(decomposed_vector[words][0])
                            feature.append(decomposed_vector[words][1])

                        feature.append(float(date[0:4])) # append YEAR
                        feature.append(float(date[4:6])) # append MONTH
                        feature.append(float(date[6:8])) # append DATE

                        sentiment_score = self.sia.polarity_scores(selected_sentiment)['compound']

                        feature.append(sentiment_score)

                        feature += stock_info[date]

                        features.append(np.array(feature))

        features = np.array(features)

        features.view('<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8,<f8').sort(order=['f20','f21','f22'], axis=0)

        with open('./data_generator/output/c2v_headline_sentiment', 'w') as file:
            np.savetxt(file, features, fmt = "%s" , delimiter=",")

        return features


    def generate_c2v_content_sentiment(self):

        decomposed_vector = util.load_decomposed_vector()

        stock_info = util.get_stock_info(self.ticker)

        word_selector = Word_Selector()

        features = []

        for file in self.files:
            for row in csv.reader(open(file, 'r', encoding = 'utf-8')):
                date = parser.parse(row[1]).strftime("%Y%m%d") # date = 'YYYYMMDD'
                #feature shape ===> (20, 1)
                #feature format {word_vec, ...(20).... , word_vec, sentiment_score, short_term_return, mid_term_return, long_term_return , raw_stock_open_adj_close }
                #print(date)
                if date in stock_info:
                    #print(date)
                    feature = []

                    selected_content   = word_selector.get_top_freq_with_sentiment(decomposed_vector, row[2])
                    #selected_content = [i for i in range(15)]

                    if selected_content:

                        for words in selected_content[::-1]:

                            #print("Selected Word: \"" + words[0] + "\" with sentiment: " + str(words[1]))
                            #feature.append(random.uniform(-1,1))
                            #feature.append(random.uniform(-1,1))
                            #feature.append(random.uniform(-1,1))

                            feature.append(decomposed_vector[words[0]][0]) #WORD_VEC
                            feature.append(decomposed_vector[words[0]][1]) #WORD_VEC
                            feature.append(float(words[1]))                #SENTIMENT
                        
                        feature.append(float(date[0:4])) # append YEAR
                        feature.append(float(date[4:6])) # append MONTH
                        feature.append(float(date[6:8])) # append DATE

                        feature += stock_info[date] #OPEN, HIGH, LOW, CLOSE, ADJCLOSE,VOL, S_RETURN, M_RETURN, L_RETURN

                        features.append(np.array(feature))

        features = np.array(features)

        print(features.shape)

        view_fmt = ','.join(['<f8' for i in range(57)])
        features.view(view_fmt).sort(order=['f45','f46','f47'], axis=0)

        with open('./data_generator/output/c2v_content_sentiment', 'w') as file:
            np.savetxt(file, features, fmt = "%s" , delimiter=",")

        return features

    def generate_high_dimension_w2v(self):

        google_vector = util.get_google_model()

        stock_info = util.get_stock_info(self.ticker)

        word_selector = Word_Selector()

        features = []

        for file in self.files:
            for row in csv.reader(open(file, 'r', encoding = 'utf-8')):

                date = parser.parse(row[1]).strftime("%Y%m%d") # date = 'YYYYMMDD'

                if date in stock_info:
                    #print(date)
                    feature = []

                    selected_content   =  word_selector.get_top_freq(google_vector, row[2], 30)

                    if selected_content:

                        for words in selected_content:

                            feature += list(google_vector[words])

                        for term in stock_info[date][:5]:
                            feature += [term for i in range(300)]




                        feature += [stock_info[date][6]] #OPEN, HIGH, LOW, CLOSE, ADJCLOSE, VOLUMN, S_RETURN, M_RETURN, L_RETURN

                        features.append(np.array(feature))


        features = np.array(features)

        print(features.shape)

        #view_fmt = ','.join(['<f8' for i in range(57)])
        #features.view(view_fmt).sort(order=['f45','f46','f47'], axis=0)

        with open('./data_generator/output/high_dimension_w2v_matrix', 'w') as file:
            np.savetxt(file, features, fmt = "%s" , delimiter=",")
