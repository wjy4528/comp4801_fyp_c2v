DATA_DIR = './data_generator/data/'


from nltk.sentiment.vader import SentimentIntensityAnalyzer
import collections
import nltk
from nltk.tokenize import sent_tokenize
from heapq import nlargest
import pickle
from nltk.corpus import stopwords
import math



class Word_Selector():
	def __init__(self):
		self.sia = SentimentIntensityAnalyzer()
		self.general_word_freq   = self.get_general_word_freq()
		self.financial_word_freq = self.get_financial_word_freq()

	def get_financial_word_freq(self):
		with open(DATA_DIR + 'financial_word_freq', 'rb') as ff:
			dt = pickle.load(ff)
		return dt

	def get_general_word_freq(self):
		with open(DATA_DIR + 'general_word_freq', 'rb') as ff:
			dt = pickle.load(ff)
		return dt

	def get_top_freq_with_sentiment(self, decomposed_vector ,context):

		describe_tags = ['JJ','JJR','JJS','RB','RBR','RBS','RP','VB','VBG','VBD','VBN','VBP','VBZ']

		#print("Counting frequency")
		freq = {}
		sentences = nltk.sent_tokenize(context)
		sentences = [nltk.word_tokenize(sent) for sent in sentences]
		sentences = [nltk.pos_tag(sent) for sent in sentences]

		for sentence in sentences:
			for word in sentence:
				if word[1] in ['LS', 'NN','NNS', 'NNP', 'NNPS', 'VB','VBG','VBD','VBN','VBP','VBZ']:

					#print(word[0])
					sentimental_words = [w[0] for w in sentence \
						if w[1] in describe_tags and w[0] not in set(stopwords.words('english')) and abs(sentence.index(word) - sentence.index(w)) < 20 ]
					#print(sentimental_words)
					sentimental_scores = self.sia.polarity_scores(' '.join(sentimental_words))['compound']
					#print(sentimental_scores)
					lower_word = word[0].lower()
					if word[0] not in freq:
						if self.general_word_freq[lower_word] != 0:
							freq[word[0]] = (self.financial_word_freq[lower_word]/self.general_word_freq[lower_word], sentimental_scores)
						else:
							freq[word[0]] = (self.financial_word_freq[lower_word], sentimental_scores)


		sorted_x = [(w[0], w[1][1]) for w in sorted(freq.items(), key=lambda kv: kv[1][0]) if w[0] in decomposed_vector and len(w[0]) > 1 and w[1][1] != 0]
		#print(sorted_x)
		if len(sorted_x) > 15:
			return sorted_x[-15:]
		else:
			return None




	def get_top_freq(self, decomposed_vector ,context, size):
		#print("Counting frequency")
		freq = {}
		sentences = nltk.sent_tokenize(context)
		sentences = [nltk.word_tokenize(sent) for sent in sentences]
		sentences = [nltk.pos_tag(sent) for sent in sentences]

		for sentence in sentences:
			for word in sentence:
				if word[1] in ['LS', 'NN','NNS', 'NNP', 'NNPS', 'VB','VBG','VBD','VBN','VBP','VBZ']:
					lower_word = word[0].lower()
					if word[0] not in freq:
						if self.general_word_freq[lower_word] != 0:
							freq[word[0]] = self.financial_word_freq[lower_word]/self.general_word_freq[lower_word]
						else:
							freq[word[0]] = self.financial_word_freq[lower_word]
		sorted_x = [w[0] for w in sorted(freq.items(), key=lambda kv: kv[1]) if w[0] in decomposed_vector and len(w[0]) > 1]
		if len(sorted_x) > size:
			return sorted_x[-size:]
		else:
			return None