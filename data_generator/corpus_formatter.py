DATA_DIR = './data/'

NEWS_TOPIC = ['us-economy','energy','commodities','financials','retail-consumer','currencies']


import nltk
import pickle
import num2words
import csv
import re
from nltk.tokenize import sent_tokenize
import string




class CorpusFormat():

	def read_general_word_base(self):
		f = open(DATA_DIR + "general_word_base", 'w', encoding = 'utf-8')
		for a in range(1,45):
			file_in = open( DATA_DIR + '1_billion_word_base/news.en-000'+ '{0:0=2d}'.format(a) + '-of-00100', 'r', encoding = 'utf-8')
			for each in file_in.readlines():
				text = re.sub(r"(\d+)", lambda x: num2words.num2words(int(x.group(0))),each)
				sentences = sent_tokenize(text)
				for sentence in sentences:
					sentence = sentence.translate(str.maketrans(' ', ' ', string.punctuation))
					sentence = sentence.replace('\n',' ')
					sentence = sentence.replace('’s',' ')
					sentence = sentence.replace('“',' ')
					sentence = sentence.replace('”',' ')
					sentence = sentence.lower()
					f.write(sentence + '\n')
			

	def read_finance_word_base(self):
		f = open(DATA_DIR + "financial_word_base", 'w', encoding = 'utf-8')
		for topic in NEWS_TOPIC:
			csv_in = open( DATA_DIR + 'financial_times/financial_times_' + topic + '.csv', 'r', encoding = 'utf-8')
			for row in csv.reader(csv_in):
				text = re.sub(r"(\d+)", lambda x: num2words.num2words(int(x.group(0))), row[2])
				sentences = sent_tokenize(text)
				for sentence in sentences:
					sentence = sentence.translate(str.maketrans(' ', ' ', string.punctuation))
					sentence = sentence.replace('\n',' ')
					sentence = sentence.replace('’s',' ')
					sentence = sentence.replace('“',' ')
					sentence = sentence.replace('”',' ')
					sentence = sentence.lower()
					f.write(sentence + '\n')
		self.get_financial_word_freq()			


	def get_general_word_freq(self):
		print("Reading general word frequency")
		general_file = open(DATA_DIR + "general_word_base", 'r', encoding = 'utf-8')
		general_words = nltk.word_tokenize(general_file.read())
		with open(DATA_DIR + 'general_word_freq', 'wb') as ff:
			pickle.dump(nltk.FreqDist(general_words), ff)


	def get_financial_word_freq(self):
		print("Reading financial word frequency")
		financial_file = open(DATA_DIR + "financial_word_base", 'r', encoding = 'utf-8')

		financial_words = nltk.word_tokenize(financial_file.read())
		with open(DATA_DIR + 'financial_word_freq', 'wb') as ff:
			pickle.dump(nltk.FreqDist(financial_words), ff)


