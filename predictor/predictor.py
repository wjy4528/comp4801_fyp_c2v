from keras.models import load_model
import numpy as np
import os
from sklearn.preprocessing import StandardScaler


class Predictor:

    def __init__(self, name):
        self.name = name
        dir_path = os.path.dirname(os.path.realpath(__file__))
        self.model = load_model(f"{dir_path}/models/{self.name}.h5")

    def format_data_rnn(self, input_X):
        input_X = input_X.reshape(input_X.shape[0], 1, input_X.shape[1])
        # print(input_X.shape)
        return input_X

    def predict(self, input):
        y = input[:, -3]
        X = np.delete(input, [20, 21, 22], -1)[:, :-3]
        scl_X = StandardScaler()
        scl_Y = StandardScaler()
        X = scl_X.fit_transform(X)
        scl_Y.fit(y.reshape(-1,1))
        if self.name == "rnn":
            X = self.format_data_rnn(X)
        res = self.model.predict(X)
        res = scl_Y.inverse_transform(res.reshape(1, -1))
        return res
