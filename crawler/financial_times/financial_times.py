from selenium import webdriver
from time import sleep
import json
import csv

USERNAME    = 'wjy4528@connect.hku.hk'
PASSWORD    = '960918Wjyy'

URL         = 'https://accounts.ft.com/login?location=https%3A%2F%2Fwww.ft.com'



csv_out = open('./financial_times_currencies.csv', 'a', newline = '', encoding = 'utf-8')
writer = csv.writer(csv_out)

driver = webdriver.Chrome('.\chromedriver.exe')
driver.get(URL)
driver.find_element_by_id('enter-email').send_keys(USERNAME)
driver.find_element_by_id('enter-email-next').click()
driver.find_element_by_id('enter-password').send_keys(PASSWORD)
sleep(120.0)
for page_num in range(1, 201):
	#print(page_num)
	while True:
		try:
			driver.get('https://www.ft.com/currencies?format=&page='+str(page_num))
			sleep(5.0)

			body = driver.find_element_by_id('stream')
			news_list = body.find_elements_by_class_name('js-teaser-heading-link')
			news_links = [ element.get_attribute('href') for element in news_list ]
			break
		except:
			sleep(3.0)
			continue
	print(len(news_links))
	headlines = []
	dates     = []
	contents  = []

	for link in news_links:
		headline = " "
		date = " "
		content  = " "

		if 'content' not in link:
			continue
		#print(link)
		while True:
			try:
				driver.get(link)
				sleep(1.0)
				break
			except:
				pass
		try:
			headline = driver.find_element_by_class_name('topper__headline').find_element_by_tag_name('span').text
			date = driver.find_element_by_class_name('article-info__timestamp').get_attribute('title')
			content = driver.find_element_by_class_name('article__content-body').text
		except:
			continue

		print(headline)
		print(date)
		#print(content)
		headlines.append(headline)
		dates.append(str(date))
		contents.append(content)
	
	for i in range(len(headlines)):
		writer.writerow([headlines[i],dates[i],contents[i]])

	print("page num " + str (page_num))

#driver.find_element_by_xpath("//select[@name='country']/option[text()='Hong Kong']").click()