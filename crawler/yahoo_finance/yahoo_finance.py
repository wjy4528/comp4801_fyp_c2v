#!/usr/bin/env python3

import sys
import re
import os
import time
import random
import json
import datetime
from math import log

# https://github.com/c0redumb/yahoo_quote_download/blob/master/yahoo_quote_download/yqd.py
from yqd import load_yahoo_quote



def get_next_date(day, delta, dateList):
    idx = dateList.index(day)
    if idx + delta < len(dateList):
        return dateList[idx + delta]
    else:
        return dateList[-1] 


def calc_return(ticker, date, next_date, priceSet):
    today = datetime.datetime.strptime(date, "%Y-%m-%d").strftime("%Y-%m-%d")
    nextDate = datetime.datetime.strptime(next_date, "%Y-%m-%d").strftime("%Y-%m-%d")
    try:
        return_self = (priceSet[ticker]['close'][nextDate] - priceSet[ticker]['open'][today]) / priceSet[ticker]['open'][today]
        #return_sp500 = log(priceSet['^GSPC']['adjClose'][date]) - log(priceSet['^GSPC']['open'][date]) 
        return True, round(return_self,  4) # relative return
    except Exception:
        return False, 0
    

def create_label(raw_price_file, return_file):
    with open(raw_price_file) as file:
        print("Loading price info ...")
        priceSet = json.load(file)
        dateSet = priceSet['^GSPC']['adjClose'].keys()

    returns = {'short': {}, 'mid': {}, 'long': {}} # 1-depth dictionary
    for num, ticker in enumerate(priceSet):
        print(num, ticker)
        for term in ['short', 'mid', 'long']:
            returns[term][ticker] = {} # 2-depth dictionary
        for day in dateSet:

            date = datetime.datetime.strptime(day, "%Y-%m-%d").strftime("%Y%m%d") # change date 2014-01-01 to 20140101
            

            tag_short, return_short = calc_return(ticker, day, get_next_date(day, 1, list(dateSet)), priceSet)
            tag_mid, return_mid = calc_return(ticker, day, get_next_date(day, 5, list(dateSet)), priceSet)
            tag_long, return_long = calc_return(ticker, day, get_next_date(day, 20, list(dateSet)), priceSet)
            
            if tag_short:
                returns['short'][ticker][date] = return_short
            if tag_mid:
                returns['mid'][ticker][date] = return_mid
            if tag_long:
                returns['long'][ticker][date] = return_long

    with open(return_file, 'w') as outfile:
        json.dump(returns, outfile, indent=4)


def get_stock_prices(infile, outfile, return_file):
    if not os.path.isfile(infile):
        raise FileNotFoundError

    fin = open(infile)
    price_set = {}
    price_set['^GSPC'] = repeat_download('^GSPC') # download S&P 500

    for num, line in enumerate(fin):
        ticker = line.strip()
        print(num, ticker)
        price_set[ticker] = repeat_download(ticker)
        

    with open(outfile, 'w') as ofile:
        json.dump(price_set, ofile, indent=4)

    create_label(outfile, return_file)


def repeat_download(ticker, start_date='20040101', end_date='29991201'):
    repeat_times = 3 # repeat download for N times
    for i in range(repeat_times):
        try:
            time.sleep(random.uniform(3, 5))
            price_str = get_price_from_yahoo(ticker, start_date, end_date)
            if price_str: # skip loop if data is not empty
                return price_str
        except Exception as e:
            print(e)
            if i == 0:
                print(ticker, "Http error!")

def get_price_from_yahoo(ticker, start_date, end_date):
    quote = load_yahoo_quote(ticker, start_date, end_date)

    # get historical price
    ticker_price = {}
    index = ['open', 'high', 'low', 'close', 'adjClose', 'volume']

    for num, line in enumerate(quote):
        line = line.strip().split(',')
        if len(line) < 7 or num == 0:
            continue
        date = line[0]

        # check if the date type matched with the standard type
        if not re.search(r'^[12]\d{3}-[01]\d-[0123]\d$', date):
            continue
        # open, high, low, close, volume, adjClose : 1,2,3,4,5,6
        for num, type_name in enumerate(index, 1):
            try:
                ticker_price[type_name][date] = round(float(line[num]), 2)
            except:
                ticker_price[type_name] = {}
    return ticker_price

if __name__ == "__main__":
    ticker_list_file = './data/finished.reuters'
    out_put_file = './data/stockPrices_raw.json'
    return_file  = './data/stockReturns.json'
    get_stock_prices(ticker_list_file, out_put_file, return_file)
    create_label(out_put_file, return_file)
    
    