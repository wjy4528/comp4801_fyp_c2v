import numpy as np
import chainer
from time import time
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

loops = 10  # for taking average
verc_size = 10 ** np.arange(5, 9)  # i
n_samples = 10 ** np.arange(5, 9)  # j

t_choice = np.zeros((len(verc_size), len(n_samples)))
t_alias  = np.zeros((len(verc_size), len(n_samples)))
t_aliasv = np.zeros((len(verc_size), len(n_samples)))



for i in range(len(verc_size)):

    p = np.random.randint(100, size=verc_size[i])
    p = p / p.sum()
    # p = p.astype(np.float32)  # for gpu?
    c = np.random.randint(100, size=verc_size[i])

    start_time = time()
    for loop in range(loops):
        sampler = chainer.utils.WalkerAlias(p)
    end_time = time()
    e_time = end_time - start_time
    t_aliasv[i, :] = e_time / (loops * 10)


    for j in range(len(n_samples)):

        # numpy choice
        start_time = time()
        for loop in range(loops):
            res = np.random.choice(c, size=n_samples[j], p=p)
        end_time = time()
        e_time = end_time - start_time
        print('choice', e_time, n_samples[j], verc_size[i])
        t_choice[i, j] = e_time

        # chainer alisas
        start_time = time()
        for loop in range(loops):
            res = sampler.sample(n_samples[j])
            res = c[res]
        end_time = time()
        e_time = end_time - start_time
        print('alias ', e_time, n_samples[j], verc_size[i])
        t_alias[i, j] = e_time / (loops*10)



fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(30, 10))

def myax(ax, title):
    ax.set_yscale('log')
    ax.set_xscale('log')
    ax.set_ylim(1e-4, 1e4)
    ax.set_title(title)
    ax.set_xlabel('# samples')
    ax.set_ylabel('time [s]')

for i in range(len(verc_size)):
    ax[0].plot(n_samples, t_choice[i], label=verc_size[i])


myax(ax[0], 'np.random.choice')
ax[0].legend(loc='best')

ax[1].plot(n_samples, t_alias)
myax(ax[1], 'Walker Alias sampling only')


ax[2].plot(n_samples, t_alias + t_aliasv)
myax(ax[2], 'Walker Alias Initialization + sampling')


fig.savefig('thisfilename.png')
