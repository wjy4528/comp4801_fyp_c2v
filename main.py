from data_generator import DataGenerator
from predictor.predictor import Predictor
from os import listdir
from os.path import isfile, join
import argparse
import numpy


def main():
    parser = argparse.ArgumentParser(description='Predict with news')
    parser.add_argument('-model', type=str, default = 'rnn', help='model type: [rnn][mlp][svm]')
    parser.add_argument('-USER_INPUT', type=str, default = './data_generator/data/user_input.csv', help='User input file name')
    parser.add_argument('-ticker', type=str, default = 'IYE', help='Target Stock Ticker')
    parser.add_argument('-type', type=str, default = 'single', help='Generate Data Type: [single][sentiment][headline][content][highdi]')
    parser.add_argument('-NEWS_DIR', type=str, default = './data_generator/data/financial_times', help='Data Dir, default : ./data/financial_times')
    args = parser.parse_args()

    files = [join(args.NEWS_DIR, f) for f in listdir(args.NEWS_DIR) if isfile(join(args.NEWS_DIR, f))]
    
    dg = DataGenerator(args.ticker, files)

    if args.type == 'single':

        dg = DataGenerator(args.ticker, [args.USER_INPUT])      
        X = dg.generate_c2v_headline_sentiment() # return np.matrix

    elif args.type == 'sentiment':        
        X = dg.generate_sentiment_matrix()

    elif args.type == 'headline':        
        X = dg.generate_c2v_headline_sentiment()

    elif args.type == 'content':       
        X = dg.generate_c2v_content_sentiment()

    elif args.type == 'highdi':
        X = dg.generate_high_dimension_w2v()

    else:
        print('Not support type')
        raise SyntaxError

    predictor = Predictor(args.model)
    pred = predictor.predict(X)

    for pre in pred[0]:
        if pre > 0:
            print(f"precition is {pre} , stock price will increase")
        else:
            print(f"precition is {pre} , stock price will decrease")

if __name__ == '__main__':
    main()
